import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';



@Component({
  selector: 'app-produits',
  templateUrl: './produits.component.html',
  styleUrls: ['./produits.component.css']
})
export class ProduitsComponent implements OnInit {
  produits:any;
  constructor(private httpClient:HttpClient ) { }
  
  ngOnInit() {
  }
ngGetproduits()
{
  this.httpClient.get("http://localhost:8087/restproduit/all").subscribe(data=>{this.produits=data}
  ,err=>{console.log(err);})
}
}
